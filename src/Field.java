import java.util.ArrayList;
import java.util.Arrays;

public class Field {

    int x;
    int y;
    int[][] size;
    ArrayList<Ship> shipsOnField;

    public Field(int x) {
        this.x = x;
        y = 1;
        size = new int[x][1];
        shipsOnField = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Field{" +
                "size=" + Arrays.toString(size) +
                '}';
    }
}
