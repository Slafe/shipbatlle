
import java.util.Scanner;

public class Game {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int countOfTurns = 0;
        int startShipPoint = 1 + (int) ((Math.random()) * 8); // Рандомная точка начала корабля
        Field field = new Field(10);
        Ship ship = new Ship(); // Создание корабля
        ship.setLocation(new int[]{startShipPoint, startShipPoint + 1, startShipPoint + 2});
        field.shipsOnField.add(ship);
        while (gameNotEnded(field)) {
            playerTurn(field);
            countOfTurns++;
        }
        System.out.println("Игра окончена! Все корабли были уничтожены за " + countOfTurns + " ходов!");
    }

    private static void playerTurn(Field field) {
        System.out.println("Введите координату выстрела - ");
        int shot = scanner.nextInt();
        Ship.isShoted(field, shot);
    }

    private static boolean gameNotEnded(Field field) {
        if (field.shipsOnField.size() != 0) {
            return true;
        } else {
            return false;
        }
    }
}
