
import java.util.Arrays;

public class Ship {

    private int[] location;
    private int hitCount;

    public Ship() {
        hitCount = 0;
    }

    public void setLocation(int[] location) {
        this.location = location;
    }

    public static boolean isShoted(Field field, int shot) {
        Ship currentShip;
        for (int shipIndex = 0; shipIndex < field.shipsOnField.size(); shipIndex++) {
            currentShip = field.shipsOnField.get(shipIndex);
            for (int shipPointLocation = 0; shipPointLocation < currentShip.location.length; shipPointLocation++) {
                if (currentShip.location[shipPointLocation] == shot) {
                    currentShip.location[shipPointLocation] = -1;
                    System.out.println("Попадание!");
                    currentShip.hitCount++;
                    if (currentShip.hitCount == 3) {
                        field.shipsOnField.remove(currentShip);
                        System.out.println("Корабль уничтожен!");
                    }
                    return true;
                }
            }
        }
        System.out.println("Мимо!");
        return false;
    }



    @Override
    public String toString() {
        return "Корабль " + Arrays.toString(location);
    }
}
